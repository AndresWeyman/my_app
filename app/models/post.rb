class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :user
	validates_presence_of :title
	validates_length_of :title, :in => 5..30, :message => "Titulo invalido"
	validates_presence_of :body
	validates :body, length: {
    	minimum: 10,
    	maximum: 50,
    	tokenizer: lambda { |str| str.split(/\s+/) },
    	too_short: "El texto del post es muy corto",
    	too_long: "El texto del post es muy grande"
	}
	has_many :comments, dependent: :destroy
end
